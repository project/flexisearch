
Introduction:

This module provides a custom flexinode search page that allows users to search flexinode content
on a field by field basis.

This module is intended for Drupal version 4.6.x


==================
Installation:
===================

IMPORTANT: A core bug in the search module was discovered during the development of Flexisearch. This bug has been applied in the current 4.6.* branch, so any Drupal releases including 4.6.6 or higher will have the fix. Otherwise, you need to patch Drupal for this module to work.

The issue describing the bug and the patch can be found here:  http://drupal.org/node/43022


1. Upload flexisearch.module to your /MODULES/ folder.

2. Go to the ADMINISTER section of your drupal site.

3. Click on MODULES

4. Enable the flexisearch.module

5. Go to ADMINISTER -> SETTINGS -> FLEXISEARCH

6. (Optionally) insert some help text for your users or customise the navigation menu link & page title.

7. Select which flexinode fields from each content type you would like included in your search.

8. If you're installing flexisearch.module on an existing site with content, you need to refresh your database search index before using Flexisearch, so before saving your settings, 
select YES to instruct Drupal to re-index your site.

9. Click on SAVE CONFIGURATION.

10. Go to ADMINISTER -> ACCESS CONTROL and enable SEARCH FLEXINODES for specific user roles.

11. Flexisearch is now active on your site, a link will appear in the navigation menu for users with
permission to SEARCH FLEXINODES.


Hope you find it useful. 

Email me or post a message on the drupal.org site if you have any ideas on how I can improve the module.

Dublin Drupaller

gus@modernmediamuse.com